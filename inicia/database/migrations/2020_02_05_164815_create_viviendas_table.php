<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViviendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viviendas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('c_habit');
            $table->integer('c_baños');
            $table->string('colonia',200);
            $table->decimal('precio',10,2);            
            $table->decimal('tamanio',10,2);            
            $table->string('municipio',60);
            $table->string('departamento',70);
            $table->string('categoría',50);
            $table->char('negociable',2);
            $table->string('estado',50);             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viviendas');
    }
}
