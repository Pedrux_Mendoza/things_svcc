<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vivienda;

class Casas extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $casas = Vivienda::all();
        return view('casas.index', compact('casas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('casas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $casa = new Vivienda();
        $casa->c_habit = $request->numhab;
        $casa->c_baños = $request->numbath;
        $casa->colonia = $request->colonia;
        $casa->precio = $request->precio;
        $casa->tamanio = $request->tama;
        $casa->municipio = $request->municipio;
        $casa->departamento = $request->departamento;
        $casa->categoría = $request->categoria;
        $casa->negociable = $request->negocio;
        $casa->estado = $request->estado;
        $casa->save();
        return redirect()->action('Casas@index')->with('datos','Registro guardado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
