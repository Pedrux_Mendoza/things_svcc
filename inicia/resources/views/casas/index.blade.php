<!DOCTYPE html>
<html>
<head>
	<title>Mostrar Casas</title>
</head>
<body>
	<a href="<?php echo url("/casas/crear"); ?>"><button type="submit" class="btn btn-primary-outline">Agregar Casa</button></a>
	<br>
	<table border="1">
		<thead>
			<tr>
				<th>ID</th>
				<th>Total Habitaciones</th>
				<th>Total Baños</th>
				<th>Colonia</th>
				<th>Precio</th>
				<th>Tamaño</th>
				<th>Municipio</th>
				<th>Departamento</th>
				<th>Categoria</th>
				<th>Negociable</th>
				<th>Estado</th>
			</tr>
		</thead>
		<tbody>
			@foreach($casas as $row)    
				<tr>
					<td>{{$row->id}}</td>
					<td>{{$row->c_habit}}</td>
					<td>{{$row->c_baños}}</td>
					<td>{{$row->colonia}}</td>
					<td>{{$row->precio}}</td>
					<td>{{$row->tamanio}}</td>
					<td>{{$row->municipio}}</td>
					<td>{{$row->departamento}}</td>
					<td>{{$row->categoría}}</td>
					<td>{{$row->negociable}}</td>
					<td>{{$row->estado}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>