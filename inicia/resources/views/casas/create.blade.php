<!DOCTYPE html>
<html>
<head>
	<title>Nueva Vivienda</title>
</head>
<body>
	<form method="POST" action="<?php echo url("/casas/agregar"); ?>">
		@csrf
	<table>
		<tr>
			<td>Total Habitaciones:</td>
			<td><input type="number" name="numhab"></td>
		</tr>
		<tr>
			<td>Total Baños:</td>
			<td><input type="number" name="numbath"></td>
		</tr>
		<tr>
			<td>Colonia:</td>
			<td><input type="text" name="colonia"></td>
		</tr>
		<tr>
			<td>Precio:</td>
			<td><input type="number" name="precio" step="0.01"></td>
		</tr>
		<tr>
			<td>Tamaño:</td>
			<td><input type="number" name="tama" step="0.01"></td>
		</tr>
		<tr>
			<td>Municipio:</td>
			<td><input type="text" name="municipio"></td>
		</tr>
		<tr>
			<td>Departamento:</td>
			<td><input type="text" name="departamento"></td>
		</tr>
		<tr>
			<td>Categoria:</td>
			<td>
				<select name="categoria">
					<option value="Mansion">Mansion</option>>
					<option value="Normal">Normal</option>>
					<option value="Duplex">Duplex</option>>
				</select>
			</td>
		</tr>		
		<tr>
			<td>Negociable:</td>
			<td>
				<select name="negocio">
					<option value="si">SI</option>>
					<option value="no">NO</option>>
				</select>
			</td>
		</tr>
		<tr>
			<td>Estado:</td>
			<td><input type="text" name="estado"></td>
		</tr>																
	</table>
	<input type="submit" name="Enviar">
	</form>
</body>
</html>