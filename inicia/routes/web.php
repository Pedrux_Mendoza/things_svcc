<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cliente','cliente@index');
Route::get('/cliente/show','cliente@show');
Route::get('/casas','Casas@index');
Route::get('/casas/crear','Casas@create');
Route::post('/casas/agregar','Casas@store');
