<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
  <link href="{{ asset('css/fieldset.css') }}" rel="stylesheet">
</head>
<body class="sb-nav-fixed">
  <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    <a class="navbar-brand" href="{{ asset('/home') }}">Cool Sales</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
      >
    </form>
    <!-- Navbar-->
    <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>                        
          </div>
        </li>
      </ul>
    </div>
  </nav>      
  <div id="layoutSidenav">
    <div id="layoutSidenav_nav">
      <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
          <div class="nav">
            <div class="sb-sidenav-menu-heading">Cajero</div>
            <a class="nav-link" href="{{ asset('/home') }}"
            ><div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
            Inicio</a
            >
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts"
            ><div class="sb-nav-link-icon"><i class="fas fa-receipt"></i></div>
            Facturas
            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
              ></a>
              <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="layout-static.html">Generar Facturas</a><a class="nav-link" href="layout-sidenav-light.html">Mostrar</a></nav>
              </div>
            </div>
          </div>
          <div class="sb-sidenav-footer">
            <div class="small">Logged in as:</div>
            {{ Auth::user()->name }}
          </div>
        </nav>
      </div>
      <div id="layoutSidenav_content">
        <main>
          <div class="container-fluid">
            <h1 class="mt-4">Factura</h1>
            <ol class="breadcrumb mb-4">
              <li class="breadcrumb-item"><a href="{{ asset('/home') }}">Inicio</a></li>
              <li class="breadcrumb-item active">Nueva Factura</li>
            </ol>
            <div class="card mb-4">
              <div class="card-header"><i class="fas fa-file-invoice"></i> Factura</div>
              <div class="card-body">
                <form>
                  <div class="form-row">
                    <div class="form-group col-md-3">
                      <?php date_default_timezone_set('America/El_Salvador'); ?>
                      <label for="formGroupExampleInput">Fecha</label>
                      <input type="date" class="form-control" name="fechaactual" id="fechaactual" readonly value="<?php echo date("Y-m-d"); ?>">
                    </div>
                    <div class="form-group col-md-3">
                      <label for="formGroupExampleInput2">Pagos</label>
                      <select class="form-control" name="pagos" id="pagos"> 
                        <option value="">--Seleccione una opcion--</option>
                        <option value="1">Credito</option>
                        <option value="2">Contado</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="formGroupExampleInput">Modo</label>
                      <select class="form-control" name="modo" id="modo">
                        <option value="">--Seleccione su modo de pago--</option>
                        <option value="1">Efectivo</option>
                        <option value="2">Tarjeta</option>
                      </select>
                    </div>                    
                  </div>
                  <div class="form-inline form-row">
                    <div class="col-md-3">
                      <button class="btn btn-info btn-block" type="button" data-toggle="collapse" data-target="#nuevo" aria-expanded="false" aria-controls="nuevo">
                        Cliente Nuevo
                      </button>                      
                    </div>
                    <div class="col-md-3">
                      <button class="btn btn-info btn-block" type="button" data-toggle="collapse" data-target="#frecuente" aria-expanded="false" aria-controls="frecuente">
                        Cliente Frecuentes
                      </button>                       
                    </div>
                    <div class="col-md-3">
                      <button class="btn btn-primary btn-block">
                        Facturar
                      </button>                       
                    </div>
                    <div class="col-md-3">
                      <button class="btn btn-danger btn-block" type="button">
                        Borrar Facturar
                      </button>                       
                    </div>                                                       
                  </div>
                  <br>
                  <div class="form-group collapse" id="nuevo">
                    <fieldset class="the-fieldset">
                      <legend class="the-legend">Nuevo Cliente</legend>
                      <div class="form-row">
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput">Example label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput2">Another label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput">Example label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput2">Another label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                      </div> 
                    </fieldset>
                  </div>
                  <div class="form-group collapse" id="frecuente">
                    <fieldset class="the-fieldset">
                      <legend class="the-legend">Cliente Frecuente</legend>
                      <div class="form-row">
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput">Example label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput2">Another label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput">Example label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput2">Another label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                      </div> 
                    </fieldset>
                  </div>
                  <div id="hotel" for-field="pagos" for-value="1" class="accordion-body collapse">
                    <fieldset class="the-fieldset">
                      <legend class="the-legend">Credito</legend>
                      <div class="form-row">
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput">Example label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput2">Another label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput">Example label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput2">Another label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                      </div> 
                    </fieldset>
                  </div>
                  <br>
                  <div id="hotel" for-field="modo" for-value="1" class="accordion-body collapse">
                    <fieldset class="the-fieldset">
                      <legend class="the-legend">Efectivo</legend>
                      <div class="form-row">
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput">Example label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput2">Another label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput">Example label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput2">Another label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                      </div> 
                    </fieldset>
                  </div>
                  <div id="hotel" for-field="modo" for-value="2" class="accordion-body collapse">
                    <fieldset class="the-fieldset">
                      <legend class="the-legend">Tarjeta</legend>
                      <div class="form-row">
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput">Example label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput2">Another label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput">Example label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="formGroupExampleInput2">Another label</label>
                          <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                        </div>
                      </div> 
                    </fieldset>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
          <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
              <div class="text-muted">Copyright &copy; Your Website 2019</div>
              <div>
                <a href="#">Privacy Policy</a>
                &middot;
                <a href="#">Terms &amp; Conditions</a>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script>
      $('select[name="pagos"]').change(function(event){
        var selected = $(this).find('option:selected');
        var value = selected.attr("value");
        var name=  $(this).attr("name");
        var selector = '[for-field="'+name+'"]';
        $('.accordion-body'+selector).addClass('collapse');
        var selectorForValue = selector+'[for-value="'+value+'"]';
        var selectedPanel = $('.accordion-body'+ selectorForValue  );
        selectedPanel.removeClass('collapse');
      })

      $('select[name="modo"]').change(function(event){
        var selected = $(this).find('option:selected');
        var value = selected.attr("value");
        var name=  $(this).attr("name");
        var selector = '[for-field="'+name+'"]';
        $('.accordion-body'+selector).addClass('collapse');
        var selectorForValue = selector+'[for-value="'+value+'"]';
        var selectedPanel = $('.accordion-body'+ selectorForValue  );
        selectedPanel.removeClass('collapse');
      })      
    </script>       
  </body>
  </html>